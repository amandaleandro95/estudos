import React from 'react';
import whatsaapImg from '../../assets/images/icons/whatsapp.svg';
import './style.css';

function TeacherItem(){
    return(
        <article className="teacher-item">
            <header>
                <img src="https://static1.conquistesuavida.com.br/articles//4/56/84/@/18404-gente-que-da-prioridade-aos-sentimentos-article_gallery-2.jpg" alt="Antonio Jorge"/>
                <div>
                    <strong>Antonio Jorge</strong>
                    <span>Química</span>
                </div>
            </header>
            <p>Entusiasta das melhores tecnologias de química avançada.<br/><br/>
                Apaixonado por explodir coisas em laboratório e por mudar a vida das pessoas através de experiências. Mais de 200.000 pessoas já passaram por uma das minhas explosões.
            </p>
            <footer>
                <p>
                    Preço/hora
                    <strong>R$ 80,00</strong>
                </p>
                <button type="button">
                    <img src={whatsaapImg} alt="Whatsappp"/>
                    Entrar em contato
                </button>
            </footer>
        </article>
    );
}
export default TeacherItem;